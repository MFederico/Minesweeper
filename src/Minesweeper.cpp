#include "Minesweeper.hpp"
#include <iostream>

#include <iomanip>

namespace modelMS {

void Minesweeper::leftClick(const int &x, const int &y) {

  auto *m = f->getMatrix();

  if (m->get(x, y)->isBomb()) {
    playerLost();
  }

  else if (!(m->get(x, y)->isVisited()) && !(m->get(x, y)->isFlagged())) {
    countBombs(x, y);
  }
}

void Minesweeper::rightClick(const int &x, const int &y) {
  auto *m = f->getMatrix();

  if (!(m->get(x, y)->isVisited())) {
    m->get(x, y)->toggleFlagged();
  }
}

void Minesweeper::countBombs(const int &x, const int &y) {
  int i = 0, j = 0;
  int count = 0;

  auto *m = f->getMatrix();

  if (m->get(x, y)->isBomb() || m->get(x, y)->isVisited()) {
    return;
  }

  m->get(x, y)->setVisited(true);

  for (i = -1; i < 2; i++) {

    for (j = -1; j < 2; j++) {

      if (((x + i) >= 0 && (x + i) < f->getWidth()) &&
          ((y + j) >= 0 && (y + j) < f->getHeight()) && !(i == 0 && j == 0)) {

        if (m->get(x + i, y + j)->isBomb()) {
          count++;
        }
      }
    }
  }

  m->get(x, y)->setBombCount(count);

  if (count == 0) {
    for (i = -1; i < 2; i++) {

      for (j = -1; j < 2; j++) {

        if (((x + i) >= 0 && (x + i) < f->getWidth()) &&
            ((y + j) >= 0 && (y + j) < f->getHeight()) && !(i == 0 && j == 0)) {
          countBombs(x + i, y + j);
        }
      }
    }
  }
}

Matrix<Cell *, 16, 16> *Minesweeper::getMatrix() { return f->getMatrix(); }

void Minesweeper::printField(const int &width, const int &height) const {
  int x, y;

  std::cout << std::endl;

  auto *m = f->getMatrix();
  for (y = 0; y < height; y++) {

    for (x = 0; x < width; x++) {

      if (m->get(x, y)->isFlagged()) {
        std::cout << std::setw(3) << "F";
      }

      else if (m->get(x, y)->isBomb()) {
        std::cout << std::setw(3) << "B";
      }

      else if (!(m->get(x, y)->isBomb()) && !(m->get(x, y)->isVisited())) {
        std::cout << std::setw(3) << "?";
      }

      else if (!(m->get(x, y)->isBomb()) && m->get(x, y)->isVisited()) {
        std::cout << std::setw(3) << m->get(x, y)->getBombCount();
      }
    }

    std::cout << std::endl;
  }
}

void Minesweeper::playerWin() { printGeneric<>("YOU WIN!!!"); }

void Minesweeper::playerLost() { std::cout << "YOU LOSE!!!" << std::endl; }

Minesweeper::Minesweeper(const int &width1, const int &height1,
                         const int &nBombs1) {
  width = width1;
  height = height1;
  nBombs = nBombs1;
  f = new Field(width, height, nBombs);
}

Minesweeper::~Minesweeper() { delete f; }

bool Minesweeper::checkWin() const {

  auto matrix = f->getMatrix();

  for (int x = 0; x < matrix->getWidth(); x++) {
    for (int y = 0; y < matrix->getHeight(); y++) {
      if (matrix->get(x, y)->isBomb() && !(matrix->get(x, y)->isFlagged())) {
        return false;
      }
      if (!(matrix->get(x, y)->isBomb()) && !(matrix->get(x, y)->isVisited())) {
        return false;
      }
    }
  }

  return true;
}

} // namespace modelMS