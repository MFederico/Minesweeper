#include "GUI.hpp"

namespace MGUI {

void GUI::removeAll() {

  /*gtk_container_foreach(GTK_CONTAINER(table),
     (GtkCallback)g_signal_handlers_disconnect_by_func,
                        NULL);  */

  gtk_container_foreach(GTK_CONTAINER(table), (GtkCallback)gtk_widget_destroy,
                        NULL);
}

void GUI::addButton(const int &i, const int &j, GtkWidget *btn) {

  gtk_table_attach_defaults(GTK_TABLE(table), btn, j, j + 1, i, i + 1);
}

void GUI::showAll() {
  gtk_widget_show_all(window);
  gtk_main();
}

GUI::~GUI() {}

GUI::GUI(const int &width, const int &height, const int &fieldWidth1,
         const int &fieldHeight1) {

  fieldWidth = fieldWidth1;
  fieldHeight = fieldHeight1;

  gtk_init(0, NULL);

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit),
                   NULL);

  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(window), width, height);
  gtk_window_set_title(GTK_WINDOW(window), "Minesweeper");

  gtk_container_set_border_width(GTK_CONTAINER(window), 0);

  table = gtk_table_new(fieldWidth, fieldHeight, TRUE);
  gtk_table_set_row_spacings(GTK_TABLE(table), 0);
  gtk_table_set_col_spacings(GTK_TABLE(table), 0);
  gtk_container_add(GTK_CONTAINER(window), table);
  gtk_quit_add_destroy(1, GTK_OBJECT(table));
  gtk_quit_add_destroy(1, GTK_OBJECT(window));
}
} // namespace MGUI