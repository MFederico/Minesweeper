#include "Cell.hpp"

namespace modelMS {
Cell::Cell(const int &x1, const int &y1) {
  x = x1;
  y = y1;
  flagged = false;
  visited = false;
  bombCount = 0;
}

Cell::~Cell() {}

bool Cell::isBomb() const { return false; }

bool Cell::isFlagged() const { return flagged; }

bool Cell::isVisited() const { return visited; }

int Cell::getBombCount() const { return bombCount; }

void Cell::toggleFlagged() { flagged = !flagged; }

void Cell::setBombCount(const int &bombCount1) { bombCount = bombCount1; }
void Cell::setVisited(const bool &visited1) { visited = visited1; }

} // namespace modelMS