#include "Matrix.hpp"
#include "Cell.hpp"
namespace modelMS {
template <class T, int R, int C>  Matrix<T, R, C>::Matrix() {

  matrix = new T *[C];
  for (int i = 0; i < R; i++) {
    matrix[i] = new T[R];
  }
}

template <class T, int R, int C> Matrix<T, R, C>::~Matrix() {
  for (int i = 0; i < R; i++) {
    delete[] matrix[i];
  }
  delete[] matrix;
}

template <class T, int R, int C>
T Matrix<T, R, C>::get(const int &i, const int &j) const {
  return matrix[i][j];
}

template <class T, int R, int C>
void Matrix<T, R, C>::set(const int &i, const int &j, T data) {
  matrix[i][j] = data;
}

template <class T, int R, int C>  int Matrix<T, R, C>::getWidth() const {
  return C;
}

template <class T, int R, int C>  int Matrix<T, R, C>::getHeight() const {
  return R;
}

template Matrix<Cell *, 16, 16>::Matrix();
template Matrix<Cell *, 16, 16>::~Matrix();
template Cell *Matrix<Cell *, 16, 16>::get(const int &i, const int &j) const;
template void Matrix<Cell *, 16, 16>::set(const int &i, const int &j,
                                          Cell *data);
template int  Matrix<Cell *, 16, 16>::getWidth() const;
template int  Matrix<Cell *, 16, 16>::getHeight() const;

} // namespace modelMS
