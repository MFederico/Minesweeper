#include "Bomb.hpp"
#include "Cell.hpp"
namespace modelMS {
Bomb::Bomb(const int &x1, const int &y1) : Cell(x1, y1) {}

bool Bomb::isBomb() const { return true; }

Bomb::~Bomb() {}
} // namespace modelMS