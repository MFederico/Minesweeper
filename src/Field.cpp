#include "Field.hpp"
#include <Bomb.hpp>
#include <chrono>
#include <random>

namespace modelMS {

Matrix<Cell *, 16, 16> *Field::getMatrix() { return &matrix; }

Field::Field(const int &width1, const int &height1, const int &bombNumber) {

  height = height1;
  width = width1;

  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      matrix.set(i, j, nullptr);
    }
  }

  int x, y;

  auto seed = std::chrono::system_clock::now().time_since_epoch().count();

  std::minstd_rand0 lc1_generator(seed);

  std::uniform_int_distribution<int> int_distribution(0, 100);

  for (int i = 0; i < bombNumber; i++) {

    x = int_distribution(lc1_generator) % width;
    y = int_distribution(lc1_generator) % height;

    while (matrix.get(x, y) != nullptr) {
      x = int_distribution(lc1_generator) % width;
      y = int_distribution(lc1_generator) % height;
    }

    matrix.set(x, y, new Bomb(x, y));
  }

  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {

      if (matrix.get(i, j) == nullptr) {
        matrix.set(i, j, new Cell(i, j));
      }
    }
  }
}

Field::~Field() {
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      delete matrix.get(i, j);
    }
  }
}

int Field::getWidth() const { return width; }

int Field::getHeight() const { return height; }
} // namespace modelMS