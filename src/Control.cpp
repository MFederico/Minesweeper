#include "GUI.hpp"
#include "Minesweeper.hpp"
#include <cstdlib>
#include <iostream>
#include <string>

modelMS::Minesweeper *m;
MGUI::GUI *g;

// Function called when the buttons are destroyed, to free their 2 int*
void free_data(gpointer data, GClosure /**closure*/) {

  int *coordinates = (int *)data;
  delete[] coordinates;
}

// function called when the GUI buttons are clicked
//*btn must be part of the function even if it is not used (it will generate a
// warning)
void button_clicked(GtkWidget *btn, GdkEventButton *event, gpointer data) {

  int *coordinates = (int *)data;

  auto matrice = m->getMatrix();

    int width = matrice->getWidth();
   int height = matrice->getHeight();

  if (event->type == GDK_BUTTON_PRESS && event->button == 1) {

    if (m->getMatrix()->get(coordinates[0], coordinates[1])->isBomb()) {
      m->playerLost();
      delete g;
      delete m;
      (void)btn;
      std::exit(EXIT_SUCCESS);

    } else {
      m->leftClick(coordinates[0], coordinates[1]);
    }
  }

  else if (event->type == GDK_BUTTON_PRESS && event->button == 3) {
    m->rightClick(coordinates[0], coordinates[1]);

  }

  else {
    std::cout << "errore input click" << std::endl;
  }

  if (m->checkWin()) {
    m->playerWin();

    delete g;
    delete m;
    std::exit(EXIT_SUCCESS);
  }

  auto *matrix = m->getMatrix();

  GtkWidget *button;

  g->removeAll(); // here the buttons are destroyed and their 2 int* are deleted

  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      int *coordinates = new int[2];
      coordinates[0] = j;
      coordinates[1] = i;

      modelMS::Cell *c = matrix->get(j, i);

      if (c->isFlagged() && !(c->isVisited())) {
        button = gtk_button_new_with_label("F");

      }

      else {

        auto str = std::to_string(c->getBombCount());

        GdkColor color;

        if (str == "0") {
          button = gtk_button_new_with_label("");

        }

        else {
          button = gtk_button_new_with_label(str.c_str());
        }

        if (c->isVisited()) {

          gdk_color_parse("GhostWhite", &color);

          gtk_widget_modify_bg(GTK_WIDGET(button), GTK_STATE_NORMAL, &color);
          gtk_widget_modify_bg(GTK_WIDGET(button), GTK_STATE_PRELIGHT, &color);
        }
      }

      g_signal_connect_data(button, "button-press-event",
                            G_CALLBACK(button_clicked), coordinates,
                            (GClosureNotify)free_data, (GConnectFlags)0);

      g->addButton(i, j, button);
    }
  }
  g->showAll();
}

//returns the maximum number of bombs in the field
constexpr int getMaxBombs(const int& h, const int& w){
  return (w*h)/4;
}

// main method, it instantiates a new GUI and Minesweeper object and runs
int main(int argc, char **argv) {

  const int maxBombs = getMaxBombs(16,16);

  if (argc!=2) {
    std::cout << "Usage: program <number_of_bombs>" << std::endl;
   std::exit(EXIT_FAILURE);
  }

  if ((std::string(argv[1]).compare("-h")) == 0) {
    std::cout << "Usage: program <number_of_bombs>" << std::endl;
    std::cout << "<number_of_bombs> is an integer between 1 and "<<maxBombs
              << std::endl;
    std::cout << "How to play:" << std::endl;
    std::cout
        << "\t* The goal is to flag all the bombs and reveal all squares. \n \
\t* To flag a bomb right-click on a unrevealed square that you think contains a bomb, a \"F\" will appear on the cell meaning that it has been flagged\n \
\t* To unflag a cell right-click on a previously flagged cell\n \
\t* To reveal a unrevealed square left-click on it, if it\'s not a bomb the revealed square will display the number of bombs surrounding it in a 3X3 grid, but if the square is a bomb you will lose"
        << std::endl;
    std::exit(EXIT_SUCCESS);
  }
  int nBombs = std::stoi(argv[1]);

  if (nBombs <= 0 || nBombs > maxBombs) {
    std::cout
        << "Input error: the number of bombs must be an integer between 1 and "
           "50, run minesweeper -h for help"
        << std::endl;
    std::exit(EXIT_FAILURE);
  }

  m = new modelMS::Minesweeper(16, 16, nBombs);
  g = new MGUI::GUI(800, 800, 16, 16);


  GtkWidget *button;

  for (int i = 0; i < m->getMatrix()->getWidth(); i++) {
    for (int j = 0; j < m->getMatrix()->getHeight(); j++) {
      int *coordinates = new int[2];
      coordinates[0] = j;
      coordinates[1] = i;

      button = gtk_button_new_with_label("");

      g_signal_connect_data(button, "button-press-event",
                            G_CALLBACK(button_clicked), coordinates,
                            (GClosureNotify)free_data, (GConnectFlags)0);

      g->addButton(i, j, button);
    }
  }
  g->showAll();

  delete g;
  delete m;
  std::exit(EXIT_SUCCESS);
}