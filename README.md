# Minesweeper

The classic Minesweeper game, in a 16X16 field with a Gtk+2.0 GUI

### Prerequisites

g++ ( must support c++14) install with

```
sudo apt-get install g++
sudo apt-get install build-essential
```

cmake (at least version 3.5) install with

```
sudo apt-get install cmake
```

pkg-config install with

```
sudo apt-get install pkg-config
```



Gtk+2.0 install it with 
```
sudo apt-get install gtk+-2.0
sudo apt-get install build-essential libgtk2.0-dev
```
## Building the program

Move to Project/build

Run cmake and choose which mode use to build the program (Debug or Release)

To build in debug mode use the following command
```
cmake -DCMAKE_BUILD_TYPE=Debug ..
```
To build in release mode use the following command
```
cmake -DCMAKE_BUILD_TYPE=Release ..
```
If no mode is specified the program will be built in release mode

To compile the program use the following commands 

```
cd build
cmake ..
make
make install
```

The executable will be placed in Project/bin

add execution permissions to the executable with

```
chmod +x ../bin/minesweeper
```

##Launching the program

Use the following command

```
bin/minesweeper <number_of_bombs>
```
where <number_of_bombs> is an integer between 1 and 50

Run 

```
bin/Release/minesweeper -h
```
for help
##How to play

* The goal is to flag all the bombs and reveal all squares.
* To flag a bomb right-click on a unrevealed square that you think contains a bomb, a "F" will appear on the cell meaning that it has been flagged
* To unflag a cell right-click on a previously flagged cell
* To reveal a unrevealed square left-click on it, if it's not a bomb the revealed square will display the number of bombs surrounding it in a 3X3 grid, but if the square is a bomb you will lose

## Running the tests

In order to run the tests launch
```
make test
```
In order to run a valgrind check launch
```
make valgrind
```
if valgrind is not installed run

```
sudo apt install valgrind
```


### Generate Doxygen documentation

To generate the project documentation run

```
make doc
```
it will be generated to the Project/doc directory


## Built With

* [GTK+2.0](https://www.gtk.org/) - The library used to generate the GUI

## Authors

* **Federico Moschin** VR398779
* **Filippo Mantovan** VR398882

## License

This project is licensed under the GPL License - see the [LICENSE.txt](LICENSE.txt) file for details

