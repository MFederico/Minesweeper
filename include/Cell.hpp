/**
 * @brief Cell to be placed in the field
 *
 * @file Cell.hpp
 * @author Federico Moschin - Filippo Mantovan
 * @date 2018-05-29
 */
#pragma once

namespace modelMS {
class Cell {
public:
  /**
   * @brief Construct a new Cell object
   *
   * @param x1 x coordinate of the cell
   * @param y1 y coordinate of the cell
   */
  explicit Cell(const int &x1, const int &y1);

  /**
   * @brief Destroy the Cell object
   *
   */
  virtual ~Cell();
  /**
   * @brief returns false because this object is not a bomb
   *
   * @return true
   * @return false
   */
   virtual bool isBomb() const;
  /**
   * @brief returns true if this Cell has been flagged
   *
   * @return true
   * @return false
   */
   bool isFlagged() const;
  /**
   * @brief returns true if the cell has been visited
   *
   * @return true
   * @return false
   */
  bool isVisited() const;
  /**
   * @brief returns the number of bombs surrounding the cell in a 3X3 grid
   *
   * @return int
   */
  int getBombCount() const;
  /**
   * @brief Set the Bomb Count
   *
   * @param bombCount
   */
  void setBombCount(const int &bombCount);
  /**
   * @brief sets wether the cell has been visited or not
   *
   * @param visited
   */
  void setVisited(const bool &visited);
  /**
   * @brief sets flagged to true if it was to false, else sets flagged to false
   *
   */
   void toggleFlagged();

private:
  bool flagged;  ///< indicates if the cell has been flagged
  bool visited;  ///< indicates if the cell has been visited
  int bombCount; ///< indicates the number of bombs surrounding the cell
  int x;         ///< x coordinate of the bomb
  int y;         ///< y coordinate of the bomb
};
} // namespace modelMS