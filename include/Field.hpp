/**
 * @brief class that contains a Matrix object, used to contain cells
 *
 * @file Field.hpp
 * @author Federico Moschin - Filippo Mantovan
 * @date 2018-05-29
 */

#pragma once
#include "Cell.hpp"
#include "Matrix.hpp"

namespace modelMS {

class Field {

private:
  Matrix<Cell *, 16, 16>
      matrix; ///< templated matrix used to store cells and bombs
  int width;  ///< width of th fields
  int height; ///< height of the field

public:
  /**
   * @brief Construct a new Field object
   *
   */
  Field();
  /**
   * @brief Destroy the Field object
   *
   */
  ~Field();
  /**
   * @brief Construct a new Field object
   *
   * @param width
   * @param height
   * @param bombNumber
   */
  explicit Field(const int &width1, const int &height1, const int &bombNumber);
  /**
   * @brief Get the Matrix object
   *
   * @return Matrix<Cell *, 16, 16>*
   */
  Matrix<Cell *, 16, 16> *getMatrix();
  /**
   * @brief get the width of the field
   *
   * @return int
   */
  int getWidth() const;
  /**
   * @brief get the height of the field
   *
   * @return int
   */
  int getHeight() const;
};
} // namespace modelMS