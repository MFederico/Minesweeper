/**
 * @brief class that contains the game's logic, it uses a Field object to
 * represent the game field
 *
 * @file Minesweeper.hpp
 * @author Federico Moschin - Filippo Mantovan
 * @date 2018-05-29
 */
#pragma once
#include "Field.hpp"
#include<iostream>
namespace modelMS {

class Minesweeper {

private:
  int width;  ///< width of the field
  int height; ///< height of the field
  int nBombs; ///< number of bombs in the field
  Field *f;   ///< field object


public:
  /**
   * @brief Construct a new Minesweeper object
   *
   * @param width width of the field
   * @param height height of the field
   * @param nBombs total number of bombs to be placed in the field
   */
  explicit Minesweeper(const int &width, const int &height, const int &nBombs);
  /**
   * @brief Destroy the Minesweeper object
   *
   */
  ~Minesweeper();
  /**
   * @brief counts the bombs in a 3X3 grid surrounding the cell at position
   * (x,y)
   *
   * @param x x position of the cell
   * @param y y position of the cell
   */
  void countBombs(const int &x, const int &y);
  /**
   * @brief prints the current state of the field
   *
   * @param width
   * @param height
   */
  void printField(const int &width, const int &height) const;
  /**
   * @brief shows a message indicating that player has won the game
   *
   */
   void playerWin();
  /**
   * @brief shows a message indicating that player has lost the game
   *
   */
  static void playerLost();
  /**
   * @brief handles the left click on a cell
   *
   * @param x x position of the cell
   * @param y y position of the cell
   */
  void leftClick(const int &x, const int &y);
  /**
   * @brief handles the right click on a cell
   *
   * @param x x position of the cell
   * @param y y position of the cell
   */
  void rightClick(const int &x, const int &y);
  /**
   * @brief Get the Matrix object
   *
   * @return Matrix<Cell *, 16, 16>*
   */
  Matrix<Cell *, 16, 16> *getMatrix();

  /**
   * @brief checks if the player won the game
   *
   * @return true if player has won
   * @return false if player has still not won
   */
  bool checkWin() const;
};

/**
   * @brief prints a generic type
   * 
   * @tparam T 
   * @param s 
   */
template <typename T> void printGeneric(T s) {
  std::cout << s << std::endl;
}

} // namespace modelMS