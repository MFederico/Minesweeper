/**
 * @brief class used to display and update the user interface
 *
 * @file GUI.hpp
 * @author Federico Moschin - Filippo Mantovan
 * @date 2018-05-29
 */
#pragma once
#include <gtk/gtk.h>

namespace MGUI {

class GUI {

public:
  /**
   * @brief Construct a new GUI object
   *
   * @param width the width of the GUI window
   * @param height the height of the GUI window
   * @param fieldWidth the width of the game matrix
   * @param fieldHeight the height of the game matrix
   */
  explicit GUI(const int &width, const int &height, const int &fieldWidth,
               const int &fieldHeight);
  /**
   * @brief Destroy the GUI object
   *
   */
  ~GUI();
  /**
   * @brief updates the GUI after the player presses a button
   *
   */
  void updateGUI();
  /**
   * @brief adds a button to the GUI
   *
   * @param i x coordinate of the button
   * @param j y coordinate of the button
   * @param btn button to add
   */
  void addButton(const int &i, const int &j, GtkWidget *btn);
  /**
   * @brief shows all the components of the GUI
   *
   */
  void showAll();
  /**
   * @brief removes all the buttons from the GUI
   *
   */
  void removeAll();

private:
  int fieldWidth;    ///< width of the field
  int fieldHeight;   ///< height of the field
  GtkWidget *window; ///< window that contains the table
  GtkWidget *table;  ///< table that contains the buttons
};

} // namespace MGUI