/**
 * @brief Template class that contains a 2X2 array used to store the Cells and
 * Bombs
 *
 * @file Matrix.hpp
 * @author Federico Moschin - Filippo Mantovan
 * @date 2018-05-29
 */
#pragma once

namespace modelMS {

/**
 * @brief
 *
 * @tparam T class objects stored in the matrix
 * @tparam R number of rows in the matrix
 * @tparam C number of columns in the matrix
 */
template <class T, int R, int C> class Matrix {

private:
  T **matrix;

public:
  /**
   * @brief Construct a new Matrix object
   *
   */
  explicit Matrix();
  /**
   * @brief Destroy the Matrix object
   *
   */
  ~Matrix();

  /**
   * @brief
   *
   * @param i x position of the element in the matrix
   * @param j y position of the element in the matrix
   * @return T the element at position (i,j)
   */

  T get(const int &i, const int &j) const;

  /**
   * @brief
   *
   * @param i x position of the element to be placed in the matrix
   * @param j x position of the element to be placed in the matrix
   * @param data element to place at position (i,j)
   */

  void set(const int &i, const int &j, T data);
  /**
   * @brief Get the width of the matrix
   *
   * @return  int
   */
      int getWidth() const;
  /**
 * @brief Get the height of the matrix
 *
 * @return  int
 */
     int getHeight() const;
};

} // namespace modelMS