/**
 * @brief class that extends Cell, used to represent a bomb in the field
 *
 * @file Bomb.hpp
 * @author Federico Moschin - Filippo Mantovan
 * @date 2018-05-29
 */
#pragma once
#include "Cell.hpp"

namespace modelMS {

class Bomb : public Cell {

public:
  /**
   * @brief Construct a new Bomb object
   *
   * @param x1 x coordinate of the bomb
   * @param y1 y coordinate of the bobm
   */
  explicit Bomb(const int &x1, const int &y1);
  /**
   * @brief returns true because this object is a bomb
   *
   * @return true
   * @return false
   */
  bool isBomb() const override;
  /**
   * @brief Destroy the Bomb object
   *
   */
  ~Bomb();
};
} // namespace modelMS